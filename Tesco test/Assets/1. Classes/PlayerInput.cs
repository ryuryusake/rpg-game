﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public GameManager gameManager;
    public GameController gameController;
    public CameraFollow cameraFollow;
    public PlayerNumber playerNumber;
    public Player[] players;
    [Header("Score adjusting")]
    public float score;
    public float maxScore;
    public float scoreUp;

    [Header("Timers")]
    public float reductionSpeed = 5;
    public float playTime;

    public void FindComponents()
    {
        switch (gameManager.gameType)
        {
            case GameType.Multiplayer:
                players = GetComponentsInChildren<Player>();
                gameController = GetComponent<GameController>();
                if (cameraFollow == null)
                {
                    cameraFollow = GetComponent<CameraFollow>();
                }
                break;
        }
    }

    public void Update()
    {
        PlayGame();

        if (Input.GetButtonDown("Fire1"))
        {
            gameController.StartGame();
        }
    }

    public void FixedUpdate()
    {
        switch (GameManager.curState)
        {
            case GameState.Paused:
            case GameState.GameOver:
            case GameState.Score:
                break;
            case GameState.Playing:
                cameraFollow.Move();
                break;
        }
    }

    void PlayGame()
    {
        switch (gameManager.gameType)
        {
            case GameType.Tesco:
                switch (GameManager.curState)
                {
                    case GameState.Playing:
                        if (Input.GetButtonDown("Fire1"))
                        {
                            if (score + scoreUp < 100)
                            {
                                score += scoreUp;
                            }
                            else
                            {
                                score = 100;
                            }
                        }

                        Timer();
                        break;
                    case GameState.Paused:
                        if (Input.GetButtonDown("Jump"))
                        {
                            gameManager.SetGameToPlay();
                            gameManager.StartTimerGamePlay(playTime);
                        }
                        break;
                    case GameState.Score:
                        gameManager.snowmanManager.Activate(SnowmanManager.CheckPercentage(score, maxScore));
                        gameManager.SetGameToGameOver();
                        break;
                    case GameState.GameOver:
                        break;
                }
                switch (GameManager.curState)
                {
                    case GameState.Playing:
                        if (Input.GetButtonDown("Fire1"))
                        {
                            if (score + scoreUp < 100)
                            {
                                score += scoreUp;
                            }
                            else
                            {
                                score = 100;
                            }
                        }

                        Timer();
                        break;
                    case GameState.Paused:
                        if (Input.GetButtonDown("Jump"))
                        {
                            gameManager.SetGameToPlay();
                            gameManager.StartTimerGamePlay(playTime);
                        }
                        break;
                    case GameState.Score:
                        gameManager.snowmanManager.Activate(SnowmanManager.CheckPercentage(score, maxScore));
                        gameManager.SetGameToGameOver();
                        break;
                    case GameState.GameOver:
                        break;
                }
                break;

            case GameType.Multiplayer:
                switch (GameManager.curState)
                {
                    case GameState.Paused:
                        if(cameraFollow.target != transform)
                        {
                            cameraFollow.UpdateTarget(transform);
                        }
                        cameraFollow.StartUp();

                        if (Input.GetButtonDown("Jump"))
                        {
                            gameManager.SetGameToPlay();
                            //gameManager.StartTimerGamePlay(playTime);
                        }
                        break;
                    case GameState.Playing:
                        if (Input.GetKeyDown(KeyCode.Alpha1))
                        {
                            playerNumber = PlayerNumber.One;
                        }

                        if (Input.GetKeyDown(KeyCode.Alpha2))
                        {
                            playerNumber = PlayerNumber.Two;
                        }

                        if (Input.GetKeyDown(KeyCode.Alpha3))
                        {
                            playerNumber = PlayerNumber.Three;
                        }

                        if (Input.GetKeyDown(KeyCode.Alpha4))
                        {
                            playerNumber = PlayerNumber.Four;
                        }

                        switch (playerNumber)
                        {
                            case PlayerNumber.One:
                                if(players[0].active == Active.Inactive)
                                {
                                    for (int i = 0; i < players.Length; i++)
                                    {
                                        if (players[i].active != Active.Retired)
                                        {
                                            players[i].active = Active.Inactive;
                                        }
                                    }

                                    players[0].active = Active.Active;

                                    if (cameraFollow.target != players[0].transform)
                                    {
                                        cameraFollow.UpdateTarget(players[0].transform);
                                    }
                                }
                                break;
                            case PlayerNumber.Two:
                                if (players[1].active == Active.Inactive)
                                {
                                    for (int i = 0; i < players.Length; i++)
                                    {
                                        if (players[i].active != Active.Retired)
                                        {
                                            players[i].active = Active.Inactive;
                                        }
                                    }

                                    players[1].active = Active.Active;

                                    if (cameraFollow.target != players[1].transform)
                                    {
                                        cameraFollow.UpdateTarget(players[1].transform);
                                    }
                                }
                                break;
                            case PlayerNumber.Three:
                                if (players[2].active == Active.Inactive)
                                {
                                    for (int i = 0; i < players.Length; i++)
                                    {
                                        if(players[i].active != Active.Retired)
                                        {
                                            players[i].active = Active.Inactive;
                                        }
                                    }

                                    players[2].active = Active.Active;

                                    if (cameraFollow.target != players[2].transform)
                                    {
                                        cameraFollow.UpdateTarget(players[2].transform);
                                    }
                                }
                                break;
                            case PlayerNumber.Four:
                                if (players[3].active == Active.Inactive)
                                {
                                    for (int i = 0; i < players.Length; i++)
                                    {
                                        if (players[i].active != Active.Retired)
                                        {
                                            players[i].active = Active.Inactive;
                                        }
                                    }

                                    players[3].active = Active.Active;

                                    if (cameraFollow.target != players[3].transform)
                                    {
                                        cameraFollow.UpdateTarget(players[3].transform);
                                    }
                                }
                                break;
                        }
                        break;
                    case GameState.Score:
                    case GameState.GameOver:
                        if (Input.GetButtonDown("Jump"))
                        {
                            Reset();
                        }
                        break;
                }             
                break;
        }
    }

    private void Reset()
    {
        foreach (Infected i in gameController.infectedManager.curinfected)
        {
            i.DestroyMe();
        }

        foreach (Player p in players)
        {
            p.active = Active.Inactive;
            if (p.hasIt)
            {
                p.hasIt = false;
            }
        }

        gameManager.SetGameToPaused();
    }

    void Timer()
    {
        if(score <= 0)
        {

        }
        else
        {
            score -= Time.deltaTime * reductionSpeed;
        }
    }
}
