﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public Active active;

    public void Play()
    {
        switch (active)
        {
            case Active.Inactive:
                break;
            case Active.Active:
                break;
        }
    }
}

public enum Active
{
    Inactive,
    Active,
    Retired
}
