﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public string name;
    public float health, speed;

    public bool hasIt;

    public int score;
    
    public Active active;

    public void FixedUpdate()
    {
        switch (GameManager.curState)
        {
            case GameState.Playing:
                switch (active)
                {
                    case Active.Inactive:
                    case Active.Retired:
                        break;
                    case Active.Active:
                        Move();
                        break;
                }
                break;
        }
    }

    public void ScoreUp(int i)
    {
        score += i;
    }

    public void Move()
    {
        float ver = Input.GetAxis("Vertical");
        float hor = Input.GetAxis("Horizontal");

        Vector3 m = new Vector3(hor, 0, ver);
        transform.Translate(m * Time.deltaTime * speed);
    }
}
