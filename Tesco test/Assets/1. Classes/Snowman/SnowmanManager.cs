﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SnowmanManager : MonoBehaviour
{
    public List<Snowman> snowmen;
    public Snowman[] snow;
    public float waitTime;

    public void FindComponents()
    {
        //snow = GetComponentsInChildren<Snowman>();

        foreach(Snowman s in snow)
        {
            if(!snowmen.Contains(s))
            {
                snowmen.Add(s);
            }
        }
       
        Array.Clear(snow, 0, snow.Length);
    }

    public void Activate(float f)
    {
        float c = (f / 100) * snowmen.Count;
        Debug.Log(c);

        for(int i = 0; i < snowmen.Count; i++)
        {
            snowmen[i].Action("Snowmen Surprised");
        }

        for(int i = 0; i < Mathf.RoundToInt(c); i++)
        {
            TimerSnowmen(waitTime,snowmen[i], snowmen[i].happy);
            snowmen[i].Action("Snowmen Happy");
        }

        if (c >= snowmen.Count - 1)
        {
            PlayerAnimation();
        }
        else
        {
            PlayerFailedAnimation(Mathf.RoundToInt(c));
        }
    }

    public void PlayerFailedAnimation(int c)
    {
        for(int i = c; i < snowmen.Count; i++)
        {
            TimerSnowmen(waitTime, snowmen[i], snowmen[i].sad);
            snowmen[i].Action("Snowmen Sad");
            //snowmen[i].Action(snowmen[i].sad);
        }

        //TODO Put animation in here
        Debug.Log("Could be better");
    }

    public void PlayerAnimation()
    {
        //TODO Put animation in here
        Debug.Log("Perfect");
    }

    static public float CheckPercentage(float cur, float max)
    {
        float f;

        f = (cur / max) * 100;

        return f;
    }

    static public float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    private IEnumerator TimerSnowmen(float f, Snowman s, Animation a)
    {
        yield return new WaitForSeconds(f);

        s.Action(a);
    }
}
