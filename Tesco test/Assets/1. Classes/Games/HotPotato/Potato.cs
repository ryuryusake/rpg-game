﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potato : MonoBehaviour
{
    public PotatoManager potatoManager;
    public State state;

    public float timeLeft; //GLOBAL
    public Player curPlayer;

    void DestroyPlayer()
    {
        curPlayer.active = Active.Retired;
        //also add player health = 0;
        state = State.Inactive;
        Debug.Log(curPlayer.name + " has been retired");

        potatoManager.activePlayers.Remove(curPlayer);

        potatoManager.CheckStateOfGame();
    }

    public void Play()
    {
        switch (state)
        {
            case State.Paused:
            case State.Inactive:
                break;
            case State.Active:
                Timer();
                transform.position = curPlayer.transform.position;
                break;
        }
    }

    public void Timer()
    {
        if(timeLeft >= 0)
        {
            timeLeft -= Time.deltaTime;
        }
        else
        {
            DestroyPlayer();
        }
    }

    public void OnTriggerEnter(Collider c)
    {
        switch (state)
        {
            case State.Active:
                if (c.gameObject.GetComponent<Player>() && curPlayer != c.gameObject.GetComponent<Player>())
                {
                    curPlayer = c.GetComponent<Player>();
                }
                break;
        }
    }
}

public enum State
{
    Active,
    Inactive,
    Paused
}
