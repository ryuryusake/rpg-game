﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotatoManager : MonoBehaviour
{
    public GameController gameController;
    public HotPotateState state;
    public float minTime = 15, maxTime = 50; //seconds
    public Potato[] potatos;

    public List<Player> activePlayers = null;

    public void GameStart()
    {
        Debug.Log("Hot Potato started");

        foreach(Potato p in potatos)
        {
            p.state = State.Active;
        }
    }

    public void Gameplay()
    {
        switch (state)
        {
            case HotPotateState.Start:
                GameSetup();
                break;
            case HotPotateState.Paused:
                ContinueGame();
                break;
            case HotPotateState.InGame:
                GameStart();
                break;
            case HotPotateState.End:
                gameController.gameManager.SetGameToGameOver();
                state = HotPotateState.Start;
                break;
        }
    }

    public void CheckStateOfGame()
    {
        if (activePlayers.Count <= 1)
        {
            state = HotPotateState.End;
            Gameplay();
        }
        else
        {
            state = HotPotateState.Paused;
        }
    }

    public void ContinueGame()
    {
        if (activePlayers.Count > 1)
        {
            foreach (Potato p in potatos)
            {
                p.curPlayer = activePlayers[Random.Range(0, activePlayers.Count)];
            }

            RandomizeBombTimer(minTime, maxTime);

            state = HotPotateState.InGame;
            Gameplay();
        }
    }

    public void GameSetup()
    {
        //GLOBAL
        foreach (Player player in gameController.gameManager.playerInput.players)
        {
            if (player.active == Active.Active || player.active == Active.Inactive)
            {
                if (!activePlayers.Contains(player))
                {
                    activePlayers.Add(player);
                }
            }
        }

        ContinueGame();
    }

    void RandomizeBombTimer(float min, float max)
    {
        float randomTime = Random.Range(min, max);

        foreach (Potato p in potatos)
        {
            p.timeLeft = randomTime;
        }
    }
}

public enum HotPotateState
{
    Start,
    InGame,
    Paused,
    End
}
