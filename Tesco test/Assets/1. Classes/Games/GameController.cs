﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameManager gameManager;
    public Game gameMode;

    public PotatoManager potatoManager;
    public InfectedManager infectedManager;

    public void StartGame()
    {
        switch (gameMode)
        {
            case Game.HotPotato:
                potatoManager.Gameplay();
                break;
            case Game.Infected:
                infectedManager.Gameplay();
                break;
        }
    }

    public void FixedUpdate()
    {
        switch (GameManager.curState)
        {
            case GameState.Playing:
                switch (gameMode)
                {
                    case Game.HotPotato:
                        foreach (Potato p in potatoManager.potatos)
                        {
                            p.Play();
                        }
                        break;
                    case Game.Infected:
                        int playersalive = gameManager.playerInput.players.Length;
                        foreach(Player p in gameManager.playerInput.players)
                        {
                            if (p.hasIt)
                            {
                                playersalive--;
                                if(playersalive <= 1)
                                {
                                    infectedManager.state = InfectedState.End;
                                    infectedManager.Gameplay();
                                    for(int i = 0; i < gameManager.playerInput.players.Length; i++)
                                    {
                                        if (!gameManager.playerInput.players[i].hasIt)
                                        {
                                            Debug.Log(gameManager.playerInput.players[i].name + " Won the game");
                                        }
                                    }
                                }
                            }
                        }
                        break;
                }
                break;
        }
    }
}

public enum Game
{
    HotPotato,
    Infected
}
