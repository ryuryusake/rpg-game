﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfectedManager : MonoBehaviour
{
    public GameController gameController;
    public InfectedState state;
    public float minTime = 15, maxTime = 50; //seconds
    public Infected[] infecteds;
    public List<Infected> curinfected;

    public List<Player> activePlayers;

    public void GameStart()
    {
        Debug.Log("Infected started");

        foreach (Infected p in infecteds)
        {
            p.active = true;
        }

        foreach (Infected i in infecteds)
        {
            int randomnr = Random.Range(0, activePlayers.Count);

            if (!activePlayers[randomnr].hasIt)
            {
                if (!activePlayers[randomnr].gameObject.GetComponentInChildren<Infected>())
                {
                    Infected newinf = Instantiate(infecteds[0], activePlayers[randomnr].gameObject.transform.position, Quaternion.identity, activePlayers[randomnr].gameObject.transform);
                    curinfected.Add(newinf);
                    newinf.active = true;
                }

                activePlayers[randomnr].gameObject.GetComponent<Player>().hasIt = true;
                activePlayers[randomnr].gameObject.GetComponentInChildren<Infected>().active = true;
            }
        }
    }

    public void Gameplay()
    {
        switch (state)
        {
            case InfectedState.Start:
                GameSetup();
                break;
            case InfectedState.Paused:
                ContinueGame();
                break;
            case InfectedState.InGame:
                GameStart();
                break;
            case InfectedState.End:
                gameController.gameManager.SetGameToGameOver();
                state = InfectedState.Start;
                break;
        }
    }

    public void CheckStateOfGame()
    {
        if (activePlayers.Count <= 1)
        {
            state = InfectedState.End;
            Gameplay();
        }
        else
        {
            state = InfectedState.Paused;
        }
    }

    public void ContinueGame()
    {
        if (activePlayers.Count > 1)
        {
            state = InfectedState.InGame;
            Gameplay();
        }
    }

    public void GameSetup()
    {
        //GLOBAL
        foreach (Player player in gameController.gameManager.playerInput.players)
        {
            if (player.active == Active.Active || player.active == Active.Inactive)
            {
                if (!activePlayers.Contains(player))
                {
                    activePlayers.Add(player);
                }
            }
        }

        ContinueGame();
    }
}

public enum InfectedState
{
    Start,
    InGame,
    Paused,
    End
}
