﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Infected : MonoBehaviour
{
    public InfectedManager infectedManager;
    public bool active;

    public void Start()
    {
        infectedManager = FindObjectOfType<InfectedManager>();
    }

    public void OnTriggerEnter(Collider c)
    {
        if(c.gameObject.GetComponent<Player>() && active)
        {
            if (!c.gameObject.GetComponent<Player>().hasIt)
            {
                if (!c.gameObject.GetComponentInChildren<Infected>())
                {
                    Infected newinf = Instantiate(this, c.gameObject.transform.position, Quaternion.identity, c.gameObject.transform);
                    infectedManager.curinfected.Add(newinf);
                }

                c.gameObject.GetComponent<Player>().hasIt = true;
                c.gameObject.GetComponentInChildren<Infected>().active = true;
            }
        }
    }

    public void DestroyMe()
    {
        active = false;
    }
}
