﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameType gameType;
    public static GameState curState;
    public SnowmanManager snowmanManager;
    public PlayerInput playerInput;
    public delegate void StartGame();
    public StartGame startGame;
    public delegate void StartComponents();
    public StartComponents startComponents;

    private void Awake()
    {
        startGame += FindComponents; 
        startGame += SetGameToPaused;
    }

    private void Start()
    {
        startGame();

        startComponents += playerInput.FindComponents;

        switch (gameType)
        {
            case GameType.Tesco:
                startComponents += snowmanManager.FindComponents;
                break;
            case GameType.Multiplayer:
                startComponents += playerInput.FindComponents;
                break;
        }

        startComponents();
    }

    private void FindComponents()
    {
        switch (gameType)
        {
            case GameType.Tesco:
                snowmanManager = GetComponentInChildren<SnowmanManager>();
                break;
            case GameType.Multiplayer:
                break;
        }

        playerInput = GetComponentInChildren<PlayerInput>();

        playerInput.gameManager = this;
    }

    private void SetGameState(GameState newState)
    {
        curState = newState;

        Debug.Log(curState);
    }

    private IEnumerator GamePlay(float playTime)
    {
        yield return new WaitForSeconds(playTime);

        SetGameState(GameState.Score);
    }

    public void SetGameToPlay()
    {
        SetGameState(GameState.Playing);
    }

    public void SetGameToPaused()
    {
        SetGameState(GameState.Paused);
    }

    public void SetGameToGameOver()
    {
        SetGameState(GameState.GameOver);
    }

    public void StartTimerGamePlay(float f)
    {
        StartCoroutine(GamePlay(f));
    }
}

public enum GameState
{
    Paused,
    Playing,
    Score,
    GameOver
}

public enum GameType
{
    Tesco,
    Multiplayer
}

public enum PlayerNumber
{
    One,
    Two,
    Three,
    Four
}
