﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // camera will follow this object
    public Transform target;
    //camera transform
    public Transform camTransform;
    // offset between camera and target
    public Vector3 Offset;
    // change this value to get desired smoothness
    public float SmoothTime = 0.3f;

    private Vector3 velocity = Vector3.zero;

    public void StartUp()
    {
        camTransform = transform;
        //Offset = camTransform.position - target.position;
    }

    public void Move()
    {
        // update position
        Vector3 targetPosition = target.position + Offset;
        camTransform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, SmoothTime);

        // update rotation
        //transform.LookAt(Target);
    }

    public void UpdateTarget(Transform t)
    {
        target = t;
    }
}
